package com.cn.shuangzi.loginplugin.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.cn.shuangzi.loginplugin.common.socialized.ThirdPlatform;
import com.cn.shuangzi.loginplugin.common.socialized.pay.PayManager;
import com.cn.shuangzi.util.SZUtil;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;


public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
	
    private IWXAPI api;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.pay_result);
		if(ThirdPlatform.WE_CHAT_APPID == null){
			PayManager.getInstance().getOnPayListener().onPayFailed();
			finish();
			return;
		}
    	api = WXAPIFactory.createWXAPI(this, ThirdPlatform.WE_CHAT_APPID);
        api.handleIntent(getIntent(), this);
    }

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
        api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
	}

	@Override
	public void onResp(BaseResp resp) {

		if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
			SZUtil.log("resp:"+resp.transaction);
			SZUtil.log("ERR_OK:"+resp.errCode);
			switch (resp.errCode) {
				case BaseResp.ErrCode.ERR_OK:
					PayManager.getInstance().getOnPayListener().onPaySuccess();
					break;
				case BaseResp.ErrCode.ERR_USER_CANCEL:
					PayManager.getInstance().getOnPayListener().onPayCancel();
					break;
				case BaseResp.ErrCode.ERR_AUTH_DENIED:
					PayManager.getInstance().getOnPayListener().onPayFailed();
					break;
				default:
					PayManager.getInstance().getOnPayListener().onPayFailed();
					break;
			}
		}
		finish();
	}
}