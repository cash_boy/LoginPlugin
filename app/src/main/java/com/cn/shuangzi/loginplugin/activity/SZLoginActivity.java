package com.cn.shuangzi.loginplugin.activity;

import android.view.View;
import android.widget.TextView;

import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.loginplugin.R;
import com.cn.shuangzi.loginplugin.bean.SZUserInfo;
import com.cn.shuangzi.loginplugin.common.socialized.ThirdLogin;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.view.pop.BottomCtrlPop;
import com.cn.shuangzi.view.pop.FilterPop;
import com.cn.shuangzi.view.pop.common.CtrlItem;
import com.cn.shuangzi.view.shape_imgview.CustomShapeImageView;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by CN on 2018-1-8.
 */

public abstract class SZLoginActivity extends SZBaseActivity implements ThirdLogin
        .OnThirdLoginResponseListener, SZInterfaceActivity, View.OnClickListener {
    public static ThirdLogin thirdLogin;
    private TextView txtAppName;
    private BottomCtrlPop filterPop;
    private CustomShapeImageView imageViewLogo;
    private boolean isRequestingUserInfo;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_login;
    }

    @Override
    protected void onBindChildViews() {
        imageViewLogo = findViewById(R.id.imgLogoText);
        txtAppName = findViewById(R.id.txtAppName);
    }

    @Override
    protected void onBindChildListeners() {
        findViewById(R.id.imgLoginWeChat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWeChat();
            }
        });
        findViewById(R.id.btnLoginTypeMore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOtherLogin();
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        setTitleTxt(R.string.txt_login);
        showBackImgLeft(getBackImgLeft());
        txtAppName.setText(getAppName());
        try {
            imageViewLogo.setImageResource(getLogoRes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
    }

    private void showOtherLogin() {
        List<CtrlItem> ctrlItemList = new ArrayList<>();
        ctrlItemList.add(new CtrlItem("手机号码登录", "手机号码登录", false));
        filterPop = new BottomCtrlPop(this, ctrlItemList, new FilterPop.OnItemClickListener() {
            @Override
            public void onItemChecked(int position, CtrlItem ctrlItem) {
                filterPop.dismiss();
                startActivity(getPhoneLoginActivity(), null, true);
            }
        }, false);
        filterPop.showOnAnchorFromBottom(getBaseView());
    }

    private void loginQQ() {
        thirdLogin = new ThirdLogin(this, ThirdLogin.QQ_PLATFORM, this);
        thirdLogin.login();
    }

    private void loginWeChat() {
        if (SZUtil.isAppInstalled("com.tencent.mm")) {
            thirdLogin = new ThirdLogin(this, ThirdLogin.WE_CHAT_PLATFORM, this);
            thirdLogin.login();
        } else {
            SZToast.warning("您还没有安装微信!");
        }
    }

    @Override
    public void onSuccess(String thirdId) {
        if(!isRequestingUserInfo) {
            isRequestingUserInfo = true;
            showBar();
            request(SZRetrofitManager.getInstance().getSZRequest().loginThird(SZApp.getInstance().getSZAppId(), getThirdPlatform() == null ? "WECHAT" : getThirdPlatform(), thirdId), new SZCommonResponseListener() {
                @Override
                public void onResponseSuccess(String data) {
                    Observable observable = getDealTokenObservable(data);
                    if (observable == null) {
                        onGetTokenSuccess(data);
                    } else {
                        dealToken(observable);
                    }
                }

                @Override
                public void onResponseError(int errorCode, String errorMsg) {
                    isRequestingUserInfo = false;
                    closeBar();
                }
            });
        }
    }
    public void dealToken(Observable observable){
        request(observable, new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                closeBar();
                SZUserInfo userInfo = null;
                try {
                    userInfo = new Gson().fromJson(data, SZUserInfo.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                onDealTokenSuccess(userInfo,data);
            }
            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                isRequestingUserInfo = false;
                closeBar();
            }
        });
    }
    @Override
    public void onFailed() {
        closeBar();
        SZToast.warning(getString(R.string.txt_login_failed));
    }

    @Override
    public void onCancel() {
        closeBar();
        SZToast.warning(getString(R.string.txt_login_cancelled));
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (thirdLogin != null && thirdLogin.getLoginType() != null) {
//            switch (thirdLogin.getLoginType()) {
//                case ThirdLogin.QQ_PLATFORM:
//                    if (thirdLogin.getQQIUiListener() != null)
//                        Tencent.onActivityResultData(requestCode, resultCode, data, thirdLogin.getQQIUiListener());
//                    break;
//            }
//        }
//    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.txtProtocol) {
            startActivity(getProtocolUrl(), getWebViewActivity());
        } else if (i == R.id.txtPrivacy) {
            startActivity(getPrivacyUrl(), getWebViewActivity());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        thirdLogin = null;
    }

    public String getThirdPlatform() {
        return null;
    }

    public boolean isRequestingUserInfo() {
        return isRequestingUserInfo;
    }

    public void setRequestingUserInfo(boolean requestingUserInfo) {
        isRequestingUserInfo = requestingUserInfo;
    }

    public abstract Class getWebViewActivity();

    public abstract String getPrivacyUrl();

    public abstract String getProtocolUrl();

    public abstract Class<?> getPhoneLoginActivity();

    public abstract int getLogoRes();

    public abstract int getAppName();

    public abstract void onGetTokenSuccess(String token);

    public abstract Observable getDealTokenObservable(String token);

    public abstract void onDealTokenSuccess(SZUserInfo userInfo,String data);
}
