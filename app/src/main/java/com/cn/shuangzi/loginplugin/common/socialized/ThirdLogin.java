package com.cn.shuangzi.loginplugin.common.socialized;


import com.cn.shuangzi.SZBaseActivity;

/**
 * Created by CN on 2016/11/30.
 */

public class ThirdLogin {
    public static final String QQ_PLATFORM = "QQ_PLATFORM";
    public static final String WE_CHAT_PLATFORM = "WeChat_PLATFORM";
    private String loginType;
    private SZBaseActivity activity;
    private OnThirdLoginResponseListener onThirdLoginResponseListener;
//    private QQLogin loginQQ;
    private WeChatLogin loginWeChat;
    public ThirdLogin(SZBaseActivity activity, String loginType, OnThirdLoginResponseListener
            onThirdLoginResponseListener) {
        this.loginType = loginType;
        this.activity = activity;
        this.onThirdLoginResponseListener = onThirdLoginResponseListener;
    }
    public void login(){
        if(ThirdLogin.QQ_PLATFORM.equals(loginType)){
//            loginQQ = new QQLogin(activity,onThirdLoginResponseListener);
//            loginQQ.login();
        }else if(ThirdLogin.WE_CHAT_PLATFORM.equals(loginType)){
            loginWeChat = new WeChatLogin(activity,onThirdLoginResponseListener);
            loginWeChat.login();
        }
    }
//    public IUiListener getQQIUiListener(){
//        if(loginQQ!=null)
//            return loginQQ.listenerQQ;
//        return null;
//    }
    public String getLoginType() {
        return loginType;
    }


    public WeChatLogin getLoginWeChat() {
        return loginWeChat;
    }

//    public QQLogin getLoginQQ() {
//        return loginQQ;
//    }

    public SZBaseActivity getActivity() {
        return activity;
    }

    public void setActivity(SZBaseActivity activity) {
        this.activity = activity;
    }

    public interface OnThirdLoginResponseListener{
        void onSuccess(String thirdId);
        void onFailed();
        void onCancel();
    }
}
