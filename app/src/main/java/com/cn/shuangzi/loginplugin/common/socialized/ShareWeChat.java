package com.cn.shuangzi.loginplugin.common.socialized;


import android.app.Activity;
import android.graphics.Bitmap;

import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

/**
 * *******************
 * 微信分享
 *
 * @author cn
 *         *******************
 */
public class ShareWeChat {
    private IWXAPI api;
    private Activity activity;
    private static ShareWeChat weiXinShare;
    private String weiXinPackage = "com.tencent.mm";

    private ShareWeChat(Activity activity) {
        if(ThirdPlatform.WE_CHAT_APPID == null){
            throw new IllegalArgumentException("appId not init...");
        }
        this.activity = activity;
        api = WXAPIFactory.createWXAPI(activity, ThirdPlatform.WE_CHAT_APPID, true);
        api.registerApp(ThirdPlatform.WE_CHAT_APPID);
    }

    public static ShareWeChat getInstance(Activity activity) {
        if (weiXinShare == null)
            weiXinShare = new ShareWeChat(activity);
        return weiXinShare;
    }

    public boolean shareCircle(String title, String desc, String url, Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = url;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = ThirdUtil.getShareString(title);
        msg.description = ThirdUtil.getShareString(desc);
        msg.thumbData = SZUtil.bmpToByteArray(bitmap, true);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneTimeline;
        api.sendReq(req);
        return true;
    }

    public boolean shareFriend(String title, String desc, String url,Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = url;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = ThirdUtil.getShareString(title);
        msg.description = ThirdUtil.getShareString(desc);
        msg.thumbData = SZUtil.bmpToByteArray(bitmap, true);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
        return true;
    }

    public boolean shareFriend(String desc) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXWebpageObject webpage = new WXWebpageObject();
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.description = ThirdUtil.getShareString(desc);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
        return true;
    }

    public boolean shareFriend(Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXImageObject imgObj = new WXImageObject(bitmap);
        WXMediaMessage msg = new WXMediaMessage(imgObj);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("imgObj");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
        return true;
    }

    public boolean shareCircle(Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXImageObject imgObj = new WXImageObject(bitmap);
        WXMediaMessage msg = new WXMediaMessage(imgObj);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("imgObj");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneTimeline;
        api.sendReq(req);
        return true;
    }

    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

}
