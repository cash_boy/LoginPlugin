package com.cn.shuangzi.loginplugin.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class SZUserInfo implements Serializable {

    private UserDo userConsumerDo;
    private UserTokenDo userConsumerTokenDo;

    class UserDo implements Serializable {
        private String consumerAvatarUrl;
        private String consumerId;
        private String consumerNickName;
        private String consumerPhone;
        private String consumerApplicationId;
    }

    class UserTokenDo implements Serializable {
        private String uctId;
    }

    public String getAppId() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerApplicationId;
    }

    public void setAppId(String id) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerApplicationId = id;
    }

    public String getAvatar() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerAvatarUrl;
    }

    public void setAvatar(String avatar) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerAvatarUrl = avatar;
    }

    public String getId() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerId;
    }

    public void setId(String id) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerId = id;
    }

    public String getNickname() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerNickName;
    }

    public void setNickname(String nickname) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerNickName = nickname;
    }

    public String getToken() {
        if (userConsumerTokenDo == null) {
            return null;
        }
        return userConsumerTokenDo.uctId;
    }

    public void setToken(String token) {
        if (userConsumerTokenDo == null) {
            userConsumerTokenDo = new UserTokenDo();
        }
        userConsumerTokenDo.uctId = token;
    }

    public String getPhone() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerPhone;
    }

    public void setPhone(String phone) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerPhone = phone;
    }

}
