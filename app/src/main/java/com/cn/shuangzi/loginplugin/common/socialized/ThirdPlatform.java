package com.cn.shuangzi.loginplugin.common.socialized;

/**
 * Created by CN.
 */

public class ThirdPlatform {
    public static String QQ_APPID ;
    public static String WE_CHAT_APPID;
    public static void init(String wechatAppId,String qqAppId){
        WE_CHAT_APPID = wechatAppId;
        QQ_APPID = qqAppId;
    }
}
