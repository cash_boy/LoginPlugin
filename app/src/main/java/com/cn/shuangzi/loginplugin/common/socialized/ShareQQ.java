package com.cn.shuangzi.loginplugin.common.socialized;

import android.app.Activity;
import android.os.Bundle;

import com.cn.shuangzi.loginplugin.R;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.tencent.connect.share.QQShare;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

/**
 ********************
 * QQ分享
 * 好友分享和空间分享均在QQ应用中操作，
 * 空间分享不在空间应用中进行操作
 * @author cn
 ********************
 */
public class ShareQQ {
	private static Activity activity;
	private static Tencent mTencent;
	private static ShareQQ instance;
	private String qqPackage = "com.tencent.mobileqq";
	private ShareQQ(Activity activity) {
		if(ThirdPlatform.QQ_APPID == null){
			throw new IllegalArgumentException("appId not init...");
		}
		this.activity = activity;
		mTencent = Tencent.createInstance(ThirdPlatform.QQ_APPID, activity);
	}
	public static ShareQQ getInstance(Activity activity) {
		if (instance == null)
			instance = new ShareQQ(activity);
		return instance;
	}
	public boolean shareQQImg(String imgFilePath) {
		if(!SZUtil.isAppInstalled(qqPackage)){
			SZToast.warning("QQ客户端未安装");
			return false;
		}
		final Bundle params = new Bundle();
		params.putString(QQShare.SHARE_TO_QQ_IMAGE_LOCAL_URL, imgFilePath);
		params.putString(QQShare.SHARE_TO_QQ_APP_NAME, activity.getString(R.string.app_name));
		params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_IMAGE);
		params.putInt(QQShare.SHARE_TO_QQ_EXT_INT,
				QQShare.SHARE_TO_QQ_FLAG_QZONE_ITEM_HIDE);
		mTencent.shareToQQ(activity, params, new IUiListener() {
			@Override
			public void onCancel() {
			}
			@Override
			public void onError(UiError e) {
			}
			@Override
			public void onComplete(Object response) {
			}
		});
		return true;
	}
	public boolean shareQZoneImg(String imgPath) {
		if(!SZUtil.isAppInstalled(qqPackage)){
			SZToast.warning("QQ客户端未安装");
			return false;
		}
		final Bundle params = new Bundle();
		params.putString(QQShare.SHARE_TO_QQ_IMAGE_LOCAL_URL,imgPath);
		params.putString(QQShare.SHARE_TO_QQ_APP_NAME, activity.getString(R.string.app_name));
		params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE,
				QQShare.SHARE_TO_QQ_TYPE_IMAGE);
		params.putInt(QQShare.SHARE_TO_QQ_EXT_INT,
				QQShare.SHARE_TO_QQ_FLAG_QZONE_AUTO_OPEN);
		mTencent.shareToQQ(activity, params, new IUiListener() {
			@Override
			public void onCancel() {
			}
			@Override
			public void onError(UiError e) {
			}
			@Override
			public void onComplete(Object response) {
				SZUtil.log("response:"+response);
			}
		});
		return true;
	}
	public boolean shareQQ(String title,String desc,String url,String imgUrl) {
		if(!SZUtil.isAppInstalled(qqPackage)){
			SZToast.warning("QQ客户端未安装");
			return false;
		}
		final Bundle params = new Bundle();
		params.putString(QQShare.SHARE_TO_QQ_TITLE, ThirdUtil.getShareString(title));
		params.putString(QQShare.SHARE_TO_QQ_SUMMARY, ThirdUtil.getShareString(desc));
		params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, url);
		params.putString(QQShare.SHARE_TO_QQ_APP_NAME, activity.getString(R.string.app_name));
		params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL,imgUrl);
		params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE,
				QQShare.SHARE_TO_QQ_TYPE_DEFAULT);
		params.putInt(QQShare.SHARE_TO_QQ_EXT_INT,
				QQShare.SHARE_TO_QQ_FLAG_QZONE_ITEM_HIDE);
		mTencent.shareToQQ(activity, params, new IUiListener() {
			@Override
			public void onCancel() {
			}
			@Override
			public void onError(UiError e) {
			}
			@Override
			public void onComplete(Object response) {
			}
		});
		return true;
	}
	public boolean shareQZone(String title,String desc,String url,String imgUrl) {
		if(!SZUtil.isAppInstalled(qqPackage)){
			SZToast.warning("QQ客户端未安装");
			return false;
		}
		final Bundle params = new Bundle();
		params.putString(QQShare.SHARE_TO_QQ_TITLE, ThirdUtil.getShareString(title));
		params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, url);
		params.putString(QQShare.SHARE_TO_QQ_SUMMARY, ThirdUtil.getShareString(desc));
		params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL,imgUrl);
		params.putString(QQShare.SHARE_TO_QQ_APP_NAME, activity.getString(R.string.app_name));
		params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE,
				QQShare.SHARE_TO_QQ_TYPE_DEFAULT);
		params.putInt(QQShare.SHARE_TO_QQ_EXT_INT,
				QQShare.SHARE_TO_QQ_FLAG_QZONE_AUTO_OPEN);
		mTencent.shareToQQ(activity, params, new IUiListener() {
			@Override
			public void onCancel() {
			}
			@Override
			public void onError(UiError e) {
			}
			@Override
			public void onComplete(Object response) {
				SZUtil.log("response:"+response);
			}
		});
		return true;
	}

}

