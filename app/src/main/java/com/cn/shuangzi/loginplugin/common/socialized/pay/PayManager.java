package com.cn.shuangzi.loginplugin.common.socialized.pay;


import com.cn.shuangzi.SZBaseActivity;

import java.io.Serializable;

/**
 * Created by CN on 2017/3/14.
 */

public class PayManager {
    private PayType currentPayType = PayType.ALIPAY;
    private SZBaseActivity activity;
    private OnPayListener onPayListener;
    private String orderInfo;
    private static PayManager INSTANCE = null;

    public PayManager(SZBaseActivity activity, PayType currentPayType, String orderInfo, OnPayListener onPayListener) {
        this.activity = activity;
        this.currentPayType = currentPayType;
        this.orderInfo = orderInfo;
        this.onPayListener = onPayListener;
        INSTANCE = this;
    }

    public static PayManager getInstance() {
        return INSTANCE;
    }

    public void toPay() {
        switch (currentPayType) {
            case ALIPAY:
                new AliPay(activity, orderInfo, onPayListener).toPay();
                break;
            case WECHAT:
                new WeChatPay(activity, orderInfo, onPayListener).toPay();
                break;
        }
    }

    public OnPayListener getOnPayListener() {
        return onPayListener;
    }

    public void setOnPayListener(OnPayListener onPayListener) {
        this.onPayListener = onPayListener;
    }

    public interface OnPayListener extends Serializable {
        void onPaySuccess();

        void onPayCancel();

        void onPayFailed();
    }

}
